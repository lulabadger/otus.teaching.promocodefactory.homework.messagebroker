﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using System;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System.Text.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
	public class ConsumerService : BackgroundService
	{

		private readonly string _exchange = "ReceivingFromPartner.Exchange";
		private readonly ILogger<ConsumerService> _logger;
		private IModel _channel;
		private IConnection _con;

		private const string RoutingKeyGivingPromoCodeToCustomerGateway = "GivingPromoCodeToCustomerGateway";

		public IServiceProvider Services { get; }
		private readonly ConnectionFactory _connectionFactory;

		public ConsumerService(
			IOptions<RabbitMqSettings> options,
			IServiceProvider services,
			ILogger<ConsumerService> logger)
		{
			_connectionFactory = new()
			{
				UserName = options.Value.User,
				Password = options.Value.Password,
				Port = options.Value.Port,
				HostName = options.Value.Server,
				VirtualHost = options.Value.VirtualHost
			};
			Services = services;
			_logger = logger;
		}


		private IModel InitConsumer()
		{
			_con = _connectionFactory.CreateConnection();
			_channel = _con.CreateModel();
			_channel.QueueDeclare(
				queue: RoutingKeyGivingPromoCodeToCustomerGateway,
				exclusive: false,
				durable: true,
				autoDelete: false);

			_channel.ExchangeDeclare(
				exchange: _exchange,
				type: ExchangeType.Direct,
				durable: true);

			_channel.QueueBind(
				queue: RoutingKeyGivingPromoCodeToCustomerGateway,
				exchange: _exchange,
				routingKey: RoutingKeyGivingPromoCodeToCustomerGateway);

			return _channel;
		}


		public override Task StopAsync(CancellationToken cancellationToken)
		{
			return base.StopAsync(cancellationToken);
		}

		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{

			var _channel = InitConsumer();
			var consumer = new EventingBasicConsumer(_channel);
			consumer.Received += async (s, ea) =>
			{
				await GivingPromoCodeToCustomer(ea);
				_channel.BasicAck(ea.DeliveryTag, false);
			};
			_channel.BasicConsume(queue: RoutingKeyGivingPromoCodeToCustomerGateway, autoAck: false, consumer: consumer);
			return Task.CompletedTask;
		}

		private async Task GivingPromoCodeToCustomer(BasicDeliverEventArgs ea)
		{
			_logger.LogInformation(ea.RoutingKey);
			_logger.LogInformation(ea.ConsumerTag);
		
			var str = Encoding.Unicode.GetString(ea.Body.ToArray());
			_logger.LogInformation(str);

			try
			{
				var data = JsonSerializer.Deserialize<GivePromoCodeToCustomerDto>(str);
				if (data != null)
				{



					using var scope = Services.CreateScope();
					var _preferencesRepository = scope.ServiceProvider.GetRequiredService<IRepository<Preference>>();
					var _customersRepository = scope.ServiceProvider.GetRequiredService<IRepository<Customer>>();

					var _promoCodesRepository = scope.ServiceProvider.GetRequiredService<IRepository<PromoCode>>();

					//Получаем предпочтение по имени
					var preference = await _preferencesRepository.GetByIdAsync(data.PreferenceId);
					if (preference != null)
					{
						//  Получаем клиентов с этим предпочтением:
						var customers = await _customersRepository.GetWhere(d => d.Preferences.Any(x => x.Preference.Id == preference.Id));

						PromoCode promoCode = PromoCodeMapper.MapFromDto(data, preference, customers);
						await _promoCodesRepository.AddAsync(promoCode);


						_logger.LogInformation("Данные обновлены");
					}
				}
			}catch (Exception ex) { _logger.LogError(ex.ToString()); }	

		}
	}
}
