﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.WebHost.Controllers;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
	public class ConsumerService : BackgroundService
	{

		private readonly string _exchange = "ReceivingFromPartner.Exchange";
		private readonly ILogger<ConsumerService> _logger;
		private IModel _channel;
		private IConnection _con;

		private const string RoutingKeyAdministrationGateway = "AdministrationGateway";

		public IServiceProvider Services { get; }
		private readonly ConnectionFactory _connectionFactory;

		public ConsumerService(
			IOptions<RabbitMqSettings> options,
			IServiceProvider services,
			ILogger<ConsumerService> logger)
		{
			_connectionFactory = new()
			{
				UserName = options.Value.User,
				Password = options.Value.Password,
				Port = options.Value.Port,
				HostName = options.Value.Server,
				VirtualHost = options.Value.VirtualHost
			};
			Services = services;
			_logger = logger;
		}


		private IModel InitConsumer()
		{
			_con = _connectionFactory.CreateConnection();
			_channel = _con.CreateModel();
			_channel.QueueDeclare(
				queue: RoutingKeyAdministrationGateway,
				exclusive: false,
				durable: true,
				autoDelete: false);

			_channel.ExchangeDeclare(
				exchange: _exchange,
				type: ExchangeType.Direct,
				durable: true);

			_channel.QueueBind(
				queue: RoutingKeyAdministrationGateway,
				exchange: _exchange,
				routingKey: RoutingKeyAdministrationGateway);

			return _channel;
		}


		public override Task StopAsync(CancellationToken cancellationToken)
		{
			return base.StopAsync(cancellationToken);
		}

		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{

			var _channel = InitConsumer();
			var consumer = new EventingBasicConsumer(_channel);
			consumer.Received += async (s, ea) =>
			{
				await UpdateAppliedPromocodesAsync(ea);
				_channel.BasicAck(ea.DeliveryTag, false);
			};
			_channel.BasicConsume(queue: RoutingKeyAdministrationGateway, autoAck: false, consumer: consumer);
			return Task.CompletedTask;
		}

		private async Task UpdateAppliedPromocodesAsync(BasicDeliverEventArgs ea)
		{
			var str = Encoding.UTF8.GetString(ea.Body.ToArray());
			_logger.LogInformation(str);

			if (Guid.TryParse(str, out var id))
			{
				using var scope = Services.CreateScope();
				var _employeeRepository = scope.ServiceProvider.GetRequiredService<IRepository<Employee>>();
				var employee = await _employeeRepository.GetByIdAsync(id);
				if (employee != null)
				{
					employee.AppliedPromocodesCount++;
					await _employeeRepository.UpdateAsync(employee);
					_logger.LogInformation("Данные обновлены");
				}
			}
		}
	}
}
