﻿namespace Otus.Teaching.Pcf.Administration.WebHost
{
	public class RabbitMqSettings
	{
		public string User { get; set; }

		public string Password { get; set; }

		public string Server { get; set; }

		public string VirtualHost { get; set; }

		public int Port { get; set; }
	}
}
